package concurrency

import (
	"sync"
	"testing"
)

func TestPhilosopherMutex(t *testing.T) {
	table := make([]PhilosopherMutex, 5)

	var l *ForkMutex
	r := &ForkMutex{}

	start := make(chan struct{})
	wg := sync.WaitGroup{}

	for i := 0; i < len(table); i++ {
		l = r // clockwise assignment

		if i == len(table)-1 {
			r = table[0].L // last philosopher shares with first
		} else {
			r = &ForkMutex{ID: i + 1}
		}

		p := PhilosopherMutex{i, l, r}
		table[i] = p

		wg.Add(1)
		go func() {
			defer wg.Done()
			<-start
			p.Run(3)
		}()
	}

	close(start) // signal goroutines to start
	wg.Wait()    // wait for all philosophers to finish
}
